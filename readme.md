Summary
=======

Adds support for processing voice commands. Only provides a common set of functions other developers can use and provides no direct end-user features.

through a USB or serial port. An Insteon API module is required.

Additional details about this module at: https://yombo.net/Modules/yombobot

Learn about [Yombo Gateway](https://yombo.net/) or
[Get started today](https://yombo.net/Docs/Gateway/Quick_start)

Usage
=====

YomboBot provides an API to perform various activities on a Yombo Gateway. By itself, it does nothing, and requires other modules to utilize the features provided by this module.

The YomboBot accepts incoming command requests, and returns results of that command.

See below section for usage details.

API Usage
=========

YomboBot provides a very simple API. For an example of usage, view the
[Jabber Module](https://yombo.net/Modules/jabber).

To access the API, you must first locate the YomboBot module within
the gateway.

**Usage / Short Example**:

    :::python
    def init(self):
       """
       Setup your module as normal, but also locate YomboBot.
       """
       try:
           self.YomboBot = self._Modules['yombobot']
           self._startable = True
       except:
           logger.warn("Jabber module can't start due to no YomboBot module loaded.")
           self._startable = False

       self.BotReturn = getattr(self, "yomboBotReturn")

    def load(self):
       """
       If your module requires YomboBot to function, then don't start if it's
       not found!
       """
       if self._startable == False:
           return

    def YombBotSend(self, linein, someID):
       """
       This is simply a wrapper for your local module. It would be responsible
       for tracking UUID's, etc.

       Send a request to Yombo Bot.
       """
       self.msgIDs[someID] = {'msgfrom':msgfrom, 'msgto':msgto} # taken from jabber module
       self.YomboBot.incoming(self.BotReturn, linein, someID)

    def yomboBotReturn(self, lineout, someID):
       """
       You can label this function anything you want, but it's suggested to
       name it this so other module developers can know what this is for.

       This function will be called to return results of a request. The
       request returned can be correlated with 'someID' that was sent.
       """
       if someID not in self.msgIDs:
           return
       else:
           del(self.msgIDs[someID])
       # do some work...

Installation
============

Simply mark this module as being used by the gateway, and the gateway will download this module and install this module automatically.

Requirements
============

None.

License
=======

The [Yombo](https://yombo.net/) team and other contributors hopes that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See LICENSE file for full details.
