#This file was created by Yombo for use with Yombo Python Gateway automation
#software.  Details can be found at https://yombo.net
"""
Defines a basic yombo bot that can interact with various interfaces, such
as Jabber, SMS, IRC, cli, etc.

License
=======

See LICENSE file for full details.

The **Yombo** team and other contributors hopes that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See LICENSE file for full details.

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: Copyright 2012-2017 by Yombo.
:license: See LICENSE file for details.
"""
import inspect
import re
import time
from collections import OrderedDict

from yombo.core.exceptions import YomboModuleWarning
from yombo.core.log import get_logger
from yombo.core.module import YomboModule
from yombo.utils.fuzzysearch import FuzzySearch
import collections

logger = get_logger("modules.yombobot")

class YomboBot(YomboModule):
    """
    Yombo bot allows interactions for controlling devices, getting status, etc.
    
    This module is considered a "logic" module and doesn't use the standard
    message system. It's own API is defined and is used to define custom APIs
    within a Yombo Gateway.
    """
    def _init_(self, **kwargs):
        self._ModDescription = "YomboBot logic module"
        self._ModAuthor = "Mitch Schwenk @ Yombo"
        self._ModUrl = "http://www.yombo.net"

        self.components = {} #cache, slight memory hit for speed.
        self.messageIDs = OrderedDict() #track messages we sent. Used to send replies to
        #self.messageIDs = {}
                             #the original requester.

        self._sessionIDs = {} # A reference to all open connections.

        self.yomboBotCommands = FuzzySearch({
          ".help" : "This is the help summary. Additional \r\n   " \
            "details can be found by type the  associated \r\n   " \
            "command help.  Commands with * indicate lots \r\n   " \
            "of entries will be displayed.",
          ".whoami" : "Show details about about who you are.",
          ".voice commands" : "List voice commands.",
          ".time events" : "Show various events based on the sun.",
          ".debug show voicecmds" : "* Detailed information about voice commands.",
          ".status" : "* Get status for a device. Ex: .status Garage Door",
          ".devices" : "* List devices registered and active on this gateway.",
          }, .84)

    def _load_(self, **kwargs):
        self.voicecmds = self._Libraries['voicecmds']

    def _start_(self, **kwargs):
        pass
    
    def _stop_(self, **kwargs):
        pass
    
    def _unload_(self, **kwargs):
        pass
    
    def YomboBot_message_subscriptions(self, **kwargs):
        """
        hook_message_subscriptions called by the messages library to get a list of message types to be delivered here.
        
        YomboBot wants status messages to deliever to connected clients. Allows clients to get updates on device status.
        
        :param kwargs: 
        :return: 
        """
        return ['status', 'event']

    def registerConnection(self, **kwargs):
        """
        Register a new connection from/to a user. Whenever a new user makes a
        connection to the gateway using another module, that module must
        register the user connection here. This allows YomboBot to send
        messages to any connection.

        The following kwarg variables are used:

        :param source: The module reference, usually "self", when called from the module.
        :type source: module reference
        :param sessionid: A unique identifier for the connection. Usually UUID4.
        :type sessionid: string
        :param authuser: The "gateway user".  This is the username the gateway
            knows and applies permissions. 
        :type authuser: string
        :param remoteuser: Identification of the remote user, could be an IM
            name, IRC nickname, etc. This is not used internally, but is used
            for logging, "whoami" requests, or other such features.
        :type remoteuser: string
        :param remotetype: Type of remote connection. "user" or "robot"
        :type remoteuser: string
        """
        allowedUserTypes = ['user', 'robot']
        if "source" not in kwargs:
            YomboModuleWarning("No 'source' in variables supplied to YomboBot::incoming.", 100, self)
        elif "sessionid" not in kwargs:
            YomboModuleWarning("No 'sessionid' in variables supplied to YomboBot::incoming.", 100, self)
        elif "authuser" not in kwargs:
            YomboModuleWarning("No 'authuser' in variables supplied to YomboBot::incoming.", 100, self)
        elif "remoteuser" not in kwargs:
            YomboModuleWarning("No 'remoteuser' in variables supplied to YomboBot::incoming.", 100, self)
        elif "remotetype" not in kwargs:
            YomboModuleWarning("No 'remotetype' in variables supplied to YomboBot::incoming.", 100, self)
        elif kwargs["remotetype"] not in allowedUserTypes:
            YomboModuleWarning("'remotetype' must be one of: user, robot", 100, self)
        else:
            self._sessionIDs[kwargs['sessionid']] = {
                'source' : kwargs['source'],
                'authuser' : kwargs['authuser'],
                'remoteuser' : kwargs['remoteuser'],
                }

    def unregisterConnection(self, **kwargs):
        """
        Unregister a connection from/to a user. Call whenever a user disconnects
        from a module.

        The following kwarg variables are used:

        :param sessionid: A unique identifier for the connection. Usually UUID4.
        :type remoteuser: string
        """
        if "sessionid" not in kwargs:
            YomboModuleWarning("No 'sessionid' in variables supplied to YomboBot::incoming.", 100, self)
        if kwargs['sessionid'] in self._sessionIDs:
            del self._sessionIDs[kwargs['sessionid']]

    def validateConnection(self, **kwargs):
        """
        Test if a sessionID exists.

        The following kwarg variables are used:

        :param sessionid: A unique identifier for the connection. Usually UUID4.
        :type remoteuser: string
        """
        if "sessionid" not in kwargs:
            YomboModuleWarning("No 'sessionid' in variables supplied to YomboBot::incoming.", 100, self)
        if kwargs['sessionid'] in self._sessionIDs:
            return True
        return False

    def message(self, message):
        """
        Message from the yombo gateway. Standard Yombo Message.
        """
        logger.debug("YomboBot got a message: {message}", message=message.dump())
        logger.debug("Message ids: {messageIDs}", messageIDs=self.messageIDs)
        if message.msgOrigUUID in self.messageIDs:
            msg = self.messageIDs[message.msgOrigUUID]
            response = msg['response'].copy()
            response['msgStatus'] = message.msgStatus
            if message.msgStatusExtra != '':
                response['msgStatusExtra'] = message.msgStatusExtra
            if 'newStatus' in message.payload:
                response['newStatus'] = message.payload['newStatus']
            themsgid = response['msgid']
            thesessionid = response['sessionid']

            del response['msgid']
            del response['sessionid']
            self.sendToCallback(theCallback=msg['returncall'], response=response, msgid=themsgid, sessionid=thesessionid)
        if len(self.messageIDs) > 80:
            tossmeaway = self.messageIDs.popitem()
        #TODO: else: send status updates to connected users/connections.
        
    def incoming(self, **kwargs):
        """
        Call this function to handle a line of incoming text from
        somewhere. It will be processed and sent back to the source.

        The following kwarg variables are used:

        :param sessionid: A unique identifier for the connection. Must be registered already.
        :type sessionid: string
        :param returncall: A callable to return results to.
        :type returncall: string
        :param msgid: A unique ID to track message through YomboBot and calling module.
        :type returncall: string
        :param linein: A line of text that user sent in. Should be a voice_cmd or YomboBot command.
        :type linein: string
        """
        if "sessionid" not in kwargs:
            YomboModuleWarning("No 'sessionid' in variables supplied to YomboBot::incoming.", 100, self)
        elif "linein" not in kwargs:
            YomboModuleWarning("No 'linein' in variables supplied to YomboBot::incoming.", 100, self)
        elif "msgid" not in kwargs:
            YomboModuleWarning("No 'msgid' in variables supplied to YomboBot::incoming.", 100, self)
        elif kwargs['sessionid'] not in self._sessionIDs:
            YomboModuleWarning("'sessionid' is not registered within YombBot. Register first.", 100, self)

        returncall = kwargs.get('returncall', None)
        linein = kwargs['linein']
        msgid = kwargs['msgid']
        sessionid = kwargs['sessionid']

        frm = inspect.stack()[1]
        mod = inspect.getmodule(frm[0])

        if linein[0] == '.':  ## process help/local commands/debug..
            logger.info("linein1: {linein}", linein=linein)
            cmdKey = ''
            try:
              cmdKey = self.yomboBotCommands.get_key(linein)
            except:
                # now lets try for commands that have variables after them.
                #Single words...
                lineTemp = linein.split(' ')
                logger.info("linein2: {linein}", linein=lineTemp[0])
                try:
                  cmdKey = self.yomboBotCommands.get_key(str(lineTemp[0]))
                except:
                  body = "YomboBot command not found. Try '.help'. Your interactive interface (SMS, Jabber, IM, IRC, etc) may support additional commands, try :help"
                  self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
                  return
            try:              
                if cmdKey == ".help":
                    body = ''
                    for item in self.yomboBotCommands:
                        body = "\r\n".join("'%s' - %s" % (k,v) for k, v in self.yomboBotCommands.items())
                    self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
                    return
                elif cmdKey == ".voice commands":
                    if len(self.voicecmds) == 0:
                        body = "Their are no voice commands. Add a device that has commands."
                        self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
                    else:
                        for voicecommand, d in list(self.voicecmds.items()):
                            body = "Voice Command: %s" % (voicecommand)
                            self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
                    return
                elif cmdKey == ".whoami":
                    body = "Authenticated user: %s \r\n" \
                           "Remote User:  %s \r\n" % (self._sessionIDs[sessionid]['authuser'], self._sessionIDs[sessionid]['remoteuser'])
                    logger.debug("Yombobot: whoami: {body}", body=body)
                    self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
                    return
                elif cmdKey == ".time events":
                    body = "now Dusk: %s \r\n" \
                           "now Dawn: %s" % (self._States['is.dusk'], self._States['is.dawn'])
                    self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
                    return
                elif cmdKey == ".debug voice commands":
                    for voicekey, d in list(self.voicecmds.items()):
                        body = "Voice key: %s \r\n" \
                               "       Noun: %s \r\n" \
                               "       Verb: %s \r\n" \
                               "       Destination: %s \r\n" \
                               "       device_id: %s \r\n" \
                               "       cmdUUID: %s \r\n" % (voicekey, d['noun'], d['verb'], d['destination'], d['device_id'], d['cmdUUID'])
                        self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
                    return
                elif cmdKey == ".status":
                    deviceTemp = linein.split(' ')
                    deviceTemp.pop(0)
                    deviceString = ' '.join(deviceTemp)
                    logger.info("devicestring: {deviceString}", deviceString=deviceString)
                    logger.info("device: {device}", device=self._Devices[deviceString])
                    try:
                      theDevice = self._Device[deviceString]
                      logger.warn("theDevice: {theDevice}", theDevice=theDevice.status[0].status)
                      body = "Status of '%s' is: %s" % (theDevice.label, theDevice.status[0].status)
                      logger.warn("body: {body}, theDevice: {theDevice}", body=body, theDevice=theDevice)
                      if theDevice.status[0].statusextra != "":
                        body = body + "\n Extended status: %s" % (theDevice.status[0].statusextra, )
                      self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
                      return
                    except:
                      body = "Invalid device name for status. Try '.devices' to get a listing of registered devices."
                      self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
                      return
                elif cmdKey == ".devices":
                  body = "List of devices:\n" + '\n'.join(self._DevicesLibrary.list_devices())
                  self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
                  return
            except:
                body = "Error processing command request."
                self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
                return
            
            body = "Command not found. Try '.help' for list of commands."
            self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
            return

        #This splits up the incoming line looking for key/value pairs. Can be used to modify
        #the outgoing message or send params later.
        #EG: open garage notbefore=1371940872
        d = {}
        d['theSentence'] = re.sub(r'(\w+)=(\S+)',
            lambda m: d.setdefault(m.group(1).lower(), m.group(2)) and '',
            linein).strip()

        logger.info("d={d}", d=d)

        vc = self.voicecmds.search(d['theSentence'])

        if vc['valid'] is True:
            response = {'device' : vc['value']['noun'],
                        'command' : vc['value']['verb'],
                        'accuracy' : "{percent:.2%}".format(percent=vc['ratio']),
                        'sessionid' : kwargs['sessionid'], 
                        'msgid' : kwargs['msgid']
                       }

            logger.info("valid command: {response}", response=response)
            commandMessage = self.voicecmds.get_message(vc, 'yombo.gateway.modules.yombobot')

            isDelayed = False
            if 'notbefore' in d:
                try:
                  commandMessage.notBefore = int(d['notbefore'])
                  isDelayed = True
                except:
                  logger.info("notbefore found, but it's value is bad: {notbefore}", notbefore=str(d["notbefore"]))

            #delay in # of seconds
            if 'delay' in d:
                try:
                  delay = int(d['delay'])
                  if delay > 0:
                    commandMessage.notBefore = int(time.time()) + delay
                    isDelayed = True
                except:
                  logger.info("delay found, but it's value is bad: {delay}", delay=str(d["delay"]))

            if isDelayed:
              if 'maxdelay' in d:
                try:
                  commandMessage.notBefore = int(d['maxdelay'])
                except:
                  logger.info("maxdelay found, but it's value is bad: {notbefore}", notbefore=str(d["notbefore"]))



# TODO: peridocially, clean up old items!! Add a time to this dict
            self.messageIDs[commandMessage.msgUUID] = {'returncall':returncall, 'msgid':msgid, 'response':response}
#            logger.info("messageIDs: %s", self.messageIDs)
            commandMessage.send()
            return

        if vc['valid'] == False:
            accuracy = "{percent:.2%}".format(percent=vc['ratio'])
            body = "Requested command not found. Nearest matching command: '%s' (Confidence:%s) (line:%s)" %(vc['key'], accuracy, linein)
            self.sendToCallback(theCallback=returncall, body=body, msgid=msgid, sessionid=kwargs['sessionid'])
        
    def sendToCallback(self, **kwargs):
        if "theCallback" not in kwargs:
            YomboModuleWarning("No 'theCallback' in variables supplied to YomboBot::sendToCallback.", 100, self)
        elif "msgid" not in kwargs:
            YomboModuleWarning("No 'msgid' in variables supplied to YomboBot::sendToCallback.", 100, self)

        theCallback = kwargs['theCallback']
        if isinstance(theCallback, collections.Callable):
            if "body" in kwargs:
                theCallback(body=kwargs['body'], msgid=kwargs['msgid'], sessionid=kwargs['sessionid'])
            else:
                theCallback = kwargs['theCallback']
                del kwargs['theCallback']
                theCallback(response=kwargs['response'], msgid=kwargs['msgid'], sessionid=kwargs['sessionid'])

    # def sendToAPI(self, target, lineout, msgid):
    #     logger.debug("target out: {taget}", target=target)
    #     target = target.lower()
    #     if target not in self.components:
    #         try:
    #             self.components[target] = getComponent(target)
    #         except:
    #             return
    #
    #     if hasattr(self.components[target], 'yomboBot'):
    #         self.components[target].yomboBot(lineout, msgid) #defines a new API call for other modules.
